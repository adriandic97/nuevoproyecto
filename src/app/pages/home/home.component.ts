import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  /* Image Icon */
  public iconFb = '../../assets/icons/facebook.png';
  public iconIg = '../../assets/icons/instagram.png';
  public iconYt = '../../assets/icons/youtube.png';

  constructor() { }

  ngOnInit(): void {
  }

}
