import { Component, OnInit } from '@angular/core';
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';

@Component({
  selector: 'app-quienes-somos',
  templateUrl: './quienes-somos.component.html',
  styleUrls: ['./quienes-somos.component.scss']
})
export class QuienesSomosComponent implements OnInit {

  /* Rutas de Imágenes */
  private imgOne = "../../assets/image/barra.jpg";
  private imgTwo = "../../assets/image/dango.jpg";
  private imgThree = "../../assets/image/matcha.jpg";
  private imgFour = "../../assets/image/mochi.jpg";
  private imgFive = "../../assets/image/ramen.jpg";
  private imgSix = "../../assets/image/restaurante.jpg";
  private imgSeven = "../../assets/image/shouyu.jpg";
  private imgEight = "../../assets/image/takoyaki.jpg";

  public right = "../../assets/icons/right.png";
  public left = "../../assets/icons/left.png";

  public imageView: string;

  /* Count */
  public position = 0;

  /* Flags */
  public leftBtn = false;
  public rightBtn = true;

  constructor() {
    this.imageView = this.imgOne;
   }

  ngOnInit(): void {
  }

  public rightImage() {
   
    if (  this.position <= 7 ){
      this.position +=1;

      switch ( this.position ) {
        case 1:
          this.imageView = this.imgTwo;
          this.leftBtn = true;
          break
  
        case 2:
          this.imageView = this.imgThree;
          break;
            
        case 3:
          this.imageView = this.imgFour;
          break;
  
        case 4:
          this.imageView = this.imgFive;
          break;
  
        case 5:
          this.imageView = this.imgSix;
          break;
  
        case 6:
          this.imageView = this.imgSeven;
          break;
  
        case 7:
          this.imageView = this.imgEight;
          this.rightBtn = false;
          break;
  
        default:
          break;
      }
    }

  }

  public leftImage() {

    if ( this.position >= 0 ) {
      this.position -=1;

      switch ( this.position ) {
        case 0:
          this.imageView = this.imgOne;
          this.leftBtn = false;
          this.rightBtn = true;
          break;
        
        case 1:
          this.imageView = this.imgTwo;
          this.leftBtn = true;
          break
        
        case 2:
          this.imageView = this.imgThree;
          break;

        case 3:
          this.imageView = this.imgFour;
          break;

        case 4:
          this.imageView = this.imgFive;
          break;

        case 5:
          this.imageView = this.imgSix;
          break;

        case 6:
          this.imageView = this.imgSeven;
          break;

        default:
          break;
      }
    }

  }

}
