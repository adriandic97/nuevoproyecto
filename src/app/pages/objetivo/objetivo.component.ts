import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-objetivo',
  templateUrl: './objetivo.component.html',
  styleUrls: ['./objetivo.component.scss']
})
export class ObjetivoComponent implements OnInit {

  /* Image Icon */
  public iconRamen = '../../assets/icons/ramen.png';
  public iconDango = '../../assets/icons/dango.png'

  constructor() { }

  ngOnInit(): void {
  }

}
